'use strict';
function createNewUser () {
    let firstName = prompt ('Input your name');
    while (!firstName){
        firstName = prompt ('Input your name');
    };
    let lastName = prompt ('Input your last name');
    while (!lastName){
        lastName = prompt ('Input your last name');
    };
    let birthday = prompt ('Input your birthday in the format: dd.mm.yyyy');
    while (birthday.includes(',')){
        birthday = prompt ('Input the correct your birthday in the format: dd.mm.yyyy');
    };
    birthday = birthday.split ('.');
    let date = new Date();
    while ((birthday[0] > 31) || (birthday[1] > 12) || (birthday[2] > date.getFullYear(new Date)) ||  (birthday[2] < 1900 )) {
        alert ('Input a valid birthday');
        birthday = prompt ('Input your birthday in the format: dd.mm.yyyy');
        birthday = birthday.split ('.');
    };
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday,
        getAge() {
            let age =  date.getFullYear()- this.birthday[2]; 
            return age;
          },
        getPassword () {
            let firstChar = this.firstName[0].toUpperCase();
            let login = firstChar + this.lastName.toLocaleLowerCase()+ this.birthday[2];
            return login;
        },
    };
    return newUser;
}
let user = createNewUser();
console.log(user);
console.log (`Your age: ${user.getAge()}`);
console.log (`Your password: ${user.getPassword()}`);

